import React, { Fragment } from "react";
import ReactDOM from "react-dom";
import "./index.css";
import Form from "./mariabarcelona/Profile";
import App from "./App";
import { Route, BrowserRouter as Router } from "react-router-dom";
import * as serviceWorker from "./serviceWorker";

const routing = (
  <Router>
    <Fragment>
      <Route exact path="/" component={App} />
      <Route path="/profile" component={Form} />
    </Fragment>
  </Router>
);

ReactDOM.render(routing, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

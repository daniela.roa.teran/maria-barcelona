import React, { Fragment } from "react";

const Select = ({ options, onChange, label, name }) => {
  return (
    <Fragment>
      {options.map(opt => {
        return (
          <div>
            {label && <label>{label}</label>}
            <input
              className="form-check-input"
              name={name}
              type="radio"
              id={opt.id}
              value={opt.value}
              onChange={onChange}
            />
            <label className="form-check-label" htmlFor={opt.id}>
              {opt.name}
            </label>
          </div>
        );
      })}
      <style jsx>{`
        [type="radio"]:checked,
        [type="radio"]:not(:checked) {
          position: absolute;
          left: -9999px;
        }
        [type="radio"]:checked + label,
        [type="radio"]:not(:checked) + label {
          position: relative;
          padding-left: 28px;
          cursor: pointer;
          line-height: 20px;
          display: inline-block;
          color: #666;
        }
        [type="radio"]:checked + label:before,
        [type="radio"]:not(:checked) + label:before {
          content: "";
          position: absolute;
          left: 0;
          top: 0;
          width: 18px;
          height: 18px;
          border: 2px solid #757575;
          border-radius: 100%;
          background: #fff;
        }
        [type="radio"]:checked + label:after,
        [type="radio"]:not(:checked) + label:after {
          content: "";
          width: 8px;
          height: 8px;
          background: #757575;
          position: absolute;
          top: 5px;
          left: 5px;
          border-radius: 100%;
          -webkit-transition: all 0.2s ease;
          transition: all 0.2s ease;
        }
        [type="radio"]:not(:checked) + label:after {
          opacity: 0;
          -webkit-transform: scale(0);
          transform: scale(0);
        }
        [type="radio"]:checked + label:after {
          opacity: 1;
          -webkit-transform: scale(1);
          transform: scale(1);
        }
        .form-check-label {
          margin-right: 16px;
        }
      `}</style>
    </Fragment>
  );
};
export default Select;

import React, { Fragment } from "react";

const Dropdown = ({ label, id, name, value, onChange, options }) => {
  return (
    <Fragment>
      {label && <label htmlFor={id}>{label}</label>}
      <select
        id={id}
        name={name}
        value={value}
        onChange={onChange}
        className="select--base form-control"
      >
        {options.map(opt => {
          return <option value={opt.name}>{opt.name}</option>;
        })}
      </select>
      <style jsx>{`
        select.select--base.form-control:not([size]):not([multiple]) {
          height: auto;
          padding: 12px;
          border: 1px solid #696969;
        }
      `}</style>
    </Fragment>
  );
};

export default Dropdown;

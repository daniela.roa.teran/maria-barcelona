import React, { Fragment } from "react";

const Input = ({
  name,
  label,
  type,
  placeholder,
  onChange,
  value,
  id,
  errorMessage,
  ...rest
}) => {
  const inputType = type ? type : "text";
  return (
    <Fragment>
      {label && <label htmlFor={id}>{label}</label>}
      <input
        id={id}
        name={name}
        type={inputType}
        className="input--base form-control"
        placeholder={placeholder}
        onChange={onChange}
        value={value}
        {...rest}
      />
      {errorMessage && <label className="error-label">{errorMessage}</label>}
      <style jsx>{`
        .input--base {
          border: 1px solid #696969;
          border-radius: 2px;
          padding: 12px;
        }
        .error-label {
          color: #c9c9c9;
        }
        .input--base::placeholder {
          color: #c9c9c9;
          opacity: 1;
        }

        .input--base:-ms-input-placeholder {
          color: #c9c9c9;
        }
        .input--base::-ms-input-placeholder {
          color: #c9c9c9;
        }
      `}</style>
    </Fragment>
  );
};
export default Input;

import React, { Component, Fragment } from "react";
import Input from "./components/Input";
import Select from "./components/Select";
import Dropdown from "./components/Dropdown";

let profile_url =
  "https://my-json-server.typicode.com/volkz/technical-form/users";

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      password: "",
      repeatPassword: "",
      name: "",
      surname: "",
      email: "",
      repeatEmail: "",
      identification: "",
      gender: "none",
      document_type: "DNI",
      fields: {},
      errors: {},
      birth: Date(),
      modified: false
    };
  }
  onChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };
  getUsers = () => {
    fetch(profile_url, {
      method: "GET"
    })
      .then(data => data.json())
      .then(data => {
        this.matchUser(data);
      })
      .catch(err => {
        console.log(err);
      });
  };
  matchUser = users => {
    let userExists = false;
    users.map(usr => {
      if (
        this.state.identification === usr.identification &&
        this.state.document_type === usr.document_type
      ) {
        userExists = true;
        this.fillForm(usr);
      } else {
        this.createUser();
      }
      return userExists;
    });
  };
  fillForm = user => {
    if (!this.state.modified) {
      if (
        window.confirm(
          "Este usuario ya se encuentra registrado ¿Desea modificar los datos existentes?"
        )
      ) {
        this.setState({
          name: user.name,
          surname: user.surname,
          email: user.email,
          password: user.password,
          modified: true,
          repeatEmail: "",
          repeatPassword: ""
        });
      }
    } else {
      this.setState({
        modified: false
      });
      this.updateUser();
    }
  };
  createUser = () => {
    this.setState({
      modified: false
    });
    const formData = new FormData();
    let validated = this.validateOnCreate();
    if (validated) {
      if (window.confirm("Usuario no registrado ¿Desea agregarlo?")) {
        let userForm = this.serializeUser(formData);
        fetch(profile_url, {
          method: "POST",
          body: userForm
        })
          .then(response => response.json())
          .then(alert("Usuario creado exitosamente"));
      }
    } else {
      alert("Todos los campos deben ser completados");
    }
  };
  updateUser = () => {
    const formData = new FormData();
    let userForm = this.serializeUser(formData);
    let validated = this.validateOnCreate();
    if (validated) {
      fetch(profile_url, {
        method: "PUT",
        body: userForm
      }).then(response => response.json());
      alert("Usuario modificado exitosamente");
    } else {
      alert("Todos los campos deben ser completados");
    }
  };
  serializeUser = formData => {
    let birth = new Date(
      `${this.state.month}/${this.state.day}/${this.state.year}`
    );
    formData.append("name", this.state.name);
    formData.append("surname", this.state.surname);
    formData.append("email", this.state.email);
    formData.append("document_type", this.state.document_type);
    formData.append("identification", this.state.identification);
    formData.append("gender", this.state.gender);
    formData.append("password", this.state.password);
    formData.append("birth", birth);
    return formData;
  };
  handleValidation = event => {
    let errors = {};
    if (event.target.name === "repeatPassword") {
      if (this.state.password !== this.state.repeatPassword) {
        errors["password"] = "Las contraseñas deben ser iguales";
      }
    } else if (event.target.name === "repeatEmail") {
      if (this.state.email !== this.state.repeatEmail) {
        errors["email"] = "Los email deben ser iguales";
      }
    }
    this.setState({
      errors: errors
    });
  };
  validateOnCreate = () => {
    let isValidated = false;
    if (
      this.state.name &&
      this.state.surname &&
      this.state.identification &&
      this.state.email &&
      this.state.password &&
      this.state.repeatEmail &&
      this.state.repeatPassword &&
      this.state.email === this.state.repeatEmail &&
      this.state.password === this.state.repeatPassword
    ) {
      isValidated = true;
    }
    return isValidated;
  };
  render() {
    const selectOptions = [
      { name: "Hombre", id: "man", value: "man" },
      { name: "Mujer", id: "woman", value: "woman" },
      { name: "Prefiero no contestar", id: "none", value: "none" }
    ];
    const dropdownOptions = [{ name: "DNI" }, { name: "Pasaporte" }];
    return (
      <Fragment>
        <div className="container">
          <h3 className="mt-5 mb-5">Tus datos personales</h3>
          <form>
            <fieldset>
              <div className="row">
                <div className="col-12 col-sm-6 mb-2">
                  <Input
                    name="name"
                    onChange={this.onChange}
                    placeholder="Nombre"
                    value={this.state.name}
                  />
                </div>
                <div className="col-12 col-sm-6 mb-2">
                  <Input
                    name="surname"
                    onChange={this.onChange}
                    placeholder="Apellido"
                    value={this.state.surname}
                  />
                </div>
              </div>
              <div className="row mt-3">
                <div className="col-12 col-sm-6">Fecha de nacimiento</div>
                <div className="col-12 col-sm-6 d-none d-sm-block d-md-none">
                  Documento identificativo
                </div>
              </div>

              <div className="row mt-2">
                <div className="col-4 col-sm-2 ">
                  <Input
                    name="day"
                    placeholder="DD"
                    maxLength="2"
                    className="text-center"
                    onChange={this.onChange}
                  />
                </div>
                <div className="col-4 col-sm-2">
                  <Input
                    name="month"
                    placeholder="MM"
                    maxLength="2"
                    className="text-center"
                    onChange={this.onChange}
                  />
                </div>
                <div className="col-4 col-sm-2">
                  <Input
                    name="year"
                    placeholder="AAAA"
                    maxLength="4"
                    className="text-center"
                    onChange={this.onChange}
                  />
                </div>
                <div className="col-12 d-block d-sm-none mt-4 mb-2">
                  Documento identificativo
                </div>
                <div className="col-4 col-sm-2">
                  <Dropdown
                    onChange={this.onChange}
                    name="document_type"
                    value={this.state.document_type}
                    id="document_type"
                    options={dropdownOptions}
                  />
                </div>
                <div className="col-8 col-sm-4">
                  <Input
                    id="identification"
                    onChange={this.onChange}
                    value={this.state.identification}
                    name="identification"
                    className="text-center"
                  />
                </div>
              </div>
              <div className="row">
                <div className="col-12 mt-4">
                  <label>Género</label>
                </div>
              </div>
              <div className="row mt-2 mb-5">
                <div className="col-12 form-inline">
                  <Select
                    id="gselect"
                    name="gender"
                    value={this.state.gender}
                    onChange={this.onChange}
                    options={selectOptions}
                  />
                </div>
              </div>
              <div className="row mt-2">
                <div className="col-12 col-sm-6 mb-2">
                  <Input
                    type="email"
                    name="email"
                    placeholder="email@email.com"
                    onChange={this.onChange}
                    value={this.state.email}
                    errorMessage={this.state.errors["email"]}
                    onBlur={this.handleValidation}
                  />
                </div>
                <div className="col-12 col-sm-6 mb-2">
                  <Input
                    type="email"
                    name="repeatEmail"
                    placeholder="email@email.com"
                    value={this.state.repeatEmail}
                    onChange={this.onChange}
                    onBlur={this.handleValidation}
                  />
                </div>
              </div>
              <button
                type="button"
                onClick={this.getUsers}
                className="btn-separator"
              >
                Modificar tus datos
              </button>
              <h3 className="mt-5 mb-5">Contraseña</h3>
              <div className="row">
                <div className="col-12 col-sm-6 mb-2">
                  <Input
                    label="Contraseña"
                    placeholder="&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;"
                    type="password"
                    name="password"
                    id="password"
                    onChange={this.onChange}
                    value={this.state.password}
                    errorMessage={this.state.errors["password"]}
                  />
                </div>
                <div className="col-12 col-sm-6">
                  <Input
                    label="Repetir contraseña"
                    placeholder="&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;"
                    type="password"
                    name="repeatPassword"
                    onBlur={this.handleValidation}
                    onChange={this.onChange}
                    value={this.state.repeatPassword}
                  />
                </div>
              </div>
              <p className="btn-separator mt-4 mb-5">Cambiar tu constraseña</p>
            </fieldset>
          </form>
        </div>
        <style jsx>{`
          .btn-separator {
            border: none;
            background: transparent;
            border-bottom: 2px solid #ebebeb;
            text-align: right;
            font-size: 16px;
            margin: 16px 0;
            text-transform: uppercase;
            font-weight: bold;
            line-height: 40px;
            width: 100%;
            outline: none;
            cursor: pointer;
          }
          .btn-separator:active,
          .btn-separator:focus {
            outline: none;
          }
        `}</style>
      </Fragment>
    );
  }
}
export default Profile;
